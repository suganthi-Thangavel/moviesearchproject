// MovieSearchPage.jsx
import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link } from "react-router-dom";
import { DataGrid } from "@mui/x-data-grid";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import CircularProgress from "@mui/material/CircularProgress"; // Import CircularProgress
import "./MovieSearchPage.css";

const MovieSearchPage = () => {
  const [query, setQuery] = useState("");
  const [movies, setMovies] = useState([]); 
  const [loading, setLoading] = useState(false);

  const searchMovies = async (searchQuery) => {
    const url = `http://www.omdbapi.com/?apikey=e27098be&s=${searchQuery}`;

    try {
      setLoading(true);
      const res = await axios.get(url);
      if (res.data.Response === "True") {
        const movieData = res.data.Search.map((movie, index) => ({
          id: movie.imdbID,
          title: movie.Title,
          year: movie.Year,
          type: movie.Type,
          poster: movie.Poster,
        }));
        setMovies(movieData);
      } else {
        setMovies([]);
      }
      setLoading(false);
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (query.length >= 3) {
        searchMovies(query);
      } else {
        setMovies([]); 
      }
    }, 1000);

    return () => clearTimeout(delayDebounceFn);
  }, [query]);

  const handleSearchChange = (e) => {
    setQuery(e.target.value);
  };

  const columns = [
    { field: "title", headerName: "Title", width: 200 },
    { field: "year", headerName: "Year", width: 120 },
    { field: "type", headerName: "Type", width: 120 },
    {
      field: "poster",
      headerName: "Poster",
      width: 150,
      renderCell: (params) => (
        <Link to={`/details/${params.row.id}`}>
          <img
            src={params.value}
            alt={params.row.title}
            style={{ width: 100, borderRadius: 5, cursor: "pointer" }}
          />
        </Link>
      ),
    },
  ];

  return (
    <Box className="movie-search-container">
      <Typography variant="h4" className="movie-search-title" gutterBottom>
        Movie Search
      </Typography>
      <TextField
        label="Search for a movie"
        variant="outlined"
        fullWidth
        value={query}
        onChange={handleSearchChange}
        className="movie-search-input"
      />
      {loading && (
        <Box display="flex" justifyContent="center" my={4}>
          <CircularProgress />
        </Box>
      )}
      {movies.length === 0 && !loading && query.length >= 3 && (
        <Typography variant="body1" className="no-results-message">
          No results were found.
        </Typography>
      )}
      {movies.length > 0 && !loading && (
        <Box mt={4}>
          <DataGrid
            rows={movies}
            columns={columns}
            pageSizeOptions={[5, 10, 25, 50, 100]}
            pagination
            pageSize={5}
          />
        </Box>
      )}
    </Box>
  );
};

export default MovieSearchPage;
