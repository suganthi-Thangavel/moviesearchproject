import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";
import { useSelector } from "react-redux";
import axios from "axios";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

const MovieDetailsPage = () => {
  const { id } = useParams();
  const [movieDetails, setMovieDetails] = useState({});
  const [loading, setLoading] = useState(false);
  const apiKey = useSelector((state) => state.apiKey);

  const fetchMovieDetails = async () => {
    const url = `http://www.omdbapi.com/?apikey=e27098be&i=${id}`;

    try {
      setLoading(true);
      const res = await axios.get(url);
      setMovieDetails(res.data);
      setLoading(false);
    } catch (error) {
      console.error(error);
      setLoading(false);
    }
  };

  useEffect(() => {
    fetchMovieDetails();
  }, []);

  return (
    <Box>
      <Typography variant="h4" gutterBottom>
        Movie Details
      </Typography>
      <Button component={Link} to="/" variant="outlined" style={{marginBottom: '20px'}}>
        Back
      </Button>
      {loading ? (
        <Typography variant="body1">Loading...</Typography>
      ) : (
        <div>
          <Typography variant="h6">{movieDetails.Title}</Typography>
          <Typography variant="body1">
            Year of Release: {movieDetails.Year}
          </Typography>
          <Typography variant="body1">Rating: {movieDetails.Rated}</Typography>
          <Typography variant="body1">
            Runtime: {movieDetails.Runtime}
          </Typography>
          <Typography variant="body1">Genre: {movieDetails.Genre}</Typography>
          <Typography variant="body1">
            Director: {movieDetails.Director}
          </Typography>
          <Typography variant="body1">
            Writer: {movieDetails.Writer}
          </Typography>
          <Typography variant="body1">
            Actors: {movieDetails.Actors}
          </Typography>
          <Typography variant="body1">Plot: {movieDetails.Plot}</Typography>
          <img
            src={movieDetails.Poster}
            alt={movieDetails.Title}
            style={{ width: 300, marginTop: 20 }}
          />
        </div>
      )}
    </Box>
  );
};

export default MovieDetailsPage;
